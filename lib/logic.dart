import 'package:flutter/material.dart';
import 'Model/Question.dart';

class Logic{
List<Quiz> get question =>[
  Quiz(
      question:
      "Software is a product and can be manufactured using the same for technologies used  other engineering artifacts",
      answer: false),
  Quiz(
      question:
      "WebApps are a mixture of print publishing and software development, making their development outside the realm of software engineering practice.",
      answer: false),
  Quiz(
      question:
      "In its simplest form an external computing device may access cloud data services using a web browser.",
      answer: true),
  Quiz(
      question:
      "Product line software development depends the reuse of existing software components to provide software engineering leverage.",
      answer: true),
];

List<Widget> answer = [];
int index = 0;
int score = 0;

void nextQuestion(bool a) {
    if (a == question[index].answer) {
      answer.add(Icon(
        Icons.check,
        color: Colors.blue,
      ),);
      score ++;
    }
    else
      answer.add(Icon(
        Icons.close,
        color: Colors.red,
      ),);
    index++;
}
}