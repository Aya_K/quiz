import 'package:flutter/material.dart';
import 'Model/Question.dart';
import 'logic.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen();
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Logic logic = new Logic();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: logic.index == logic.question.length ?  Padding(padding: const EdgeInsets.all(30.0),
        child: Text("Your score : "+logic.score.toString(), style: TextStyle(fontSize: 20),),
      ) :
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(padding: const EdgeInsets.all(30.0),
           child: Text(logic.question[logic.index].toString(), style: TextStyle(fontSize: 20),),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                  child: Text("True"),
                  color: Colors.green,
                  onPressed: ()=>setState((){logic.nextQuestion(true);}),
              ),
              SizedBox(width: 20,),
              RaisedButton(
                  child: Text("False"),
                  color: Colors.red,
                  onPressed: () => setState((){logic.nextQuestion(false);}),),
            ],
          ),
          SizedBox(height: 50,),
          Row(
            children: logic.answer,
          )
        ],
      ),
    );
  }
}